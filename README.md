Ulundi
======

Ulundi is a fork of the Freeplane mind-mapping tool.

The name has no significant bearing on anything, even if you are a Zulu. 

This fork is a mavenized version of the code base. It also externalises a number of Ulundi/Freeplane dependencies.

Building from source
====================

Clone the external dependencies from github and 'mvn install' each before building freeplane:

* $ git clone https://github.com/sthysel/JOrtho.git 
* $ cd JOrtho; mvn install
* $ git clone https://github.com/sthysel/SimplyHTML.git
* $ cd SimplyHTML; mvn install


The Plan
========
Ulundi will be a Netbeans RCP, mavenised Mind Map tool, someday...
